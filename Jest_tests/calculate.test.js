const { calculate } = require('../calculate');

test('should be defined', () => {
  expect(calculate).toBeDefined();
});

test('3+4 = 7', () => {
  expect(calculate('+', 3, 4)).toBe(7);
});
test('8-10 = -2', () => {
  expect(calculate('-', 8, 10)).toBe(-2);
});
test('10/2 = 5', () => {
  expect(calculate('/', 10, 2)).toBe(5);
});
test('8*10 = 80', () => {
  expect(calculate('*', 8, 10)).toBe(80);
});
test('8>10 = false', () => {
  expect(calculate('>', 8, 10)).toBe(false);
});
test('8<10 = true', () => {
  expect(calculate('<', 8, 10)).toBe(true);
});
test('8=10 = false', () => {
  expect(calculate('=', 8, 10)).toBe(false);
});
test('8>=10 = false', () => {
  expect(calculate('>=', 8, 10)).toBe(false);
});
test('8<=10 = true', () => {
  expect(calculate('<=', 8, 10)).toBe(true);
});
